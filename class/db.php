<?
class wp_db__class__db extends wp_db__class__db__parent
{
	function __construct(&$D = null)
	{
        parent::{__function__}($D);
		#$this->C->setting()->get_config();
		#include('!config.php');
		#$this->D['CONFIG']['wp_db'] = $D['CONFIG']['wp_db'];
		
		foreach((array)$this->D['CONFIG']['MODUL']['D']['wp_db']['DB']['D'] AS $k => $v)
		{	
			if($v['ACTIVE']['VALUE'])
			{
				$this->db[ 0 ] = new mysqli($v['LOCALHOST']['VALUE'], $v['USER']['VALUE'], $v['PASSWORD']['VALUE'], $v['DATABASE']['VALUE']);
			}
		}
		
	}

	function query($query, $resultmode = MYSQLI_STORE_RESULT )
	{
		parent::{__function__}($query, $resultmode);
		return $this->db[0]->query($query,$resultmode);
	}
	
	function real_escape_string($escapestr)
	{
		parent::{__function__}($escapestr);
		return $this->db[0]->real_escape_string($escapestr);
	}
	
	#$P = [MAP => 'ing', 'ABC' => 'feld_abc', 'XXX' => "(SELECT ggg WHERE d = '[VALUE]')"]
	#$W = [FELD:LIKE|FELD2:IN|...] = "VALUE"
	function where_interpreter($P,$W)
	{
		$K = array_keys($P);
		$V = array_values($P);
		
		foreach((array)$W as $kW => $vW ) #AND
		{
			if(strpos($kW,'|') !== false ) #| = OR
			{
				$p = explode('|',$kW);
				$wW .= " AND (";
				for($w=0;$w<count($p);$w++)
				{
					$wW .= (($w>0)?' OR ':' '). str_replace([$p[$w]],[$vW],$P[$p[$w]]);
				}
				$wW .= ")";
			}
			else
				$wW .= ' AND '. str_replace([$kW],[$vW],$P[$kW]);
		}
		return $wW;
	}
}

#$D['USER']['D']['W'] = "EMAIL = '{$D['LOGIN']['NICK']}' OR NICK = '{$D['LOGIN']['NICK']}'";
			#$D['USER']['D']['L'] = '0,1';
			#$D['USER']['D']['O'] = 'NICK:D,EMAIL:A,ID';
			#$D['USER']['D']['G'] = 'ID,NICK';